import React, { Component } from 'react';
import { Box, Text } from '@chakra-ui/react';

export default class Header extends Component {
  render() {
    return (
     
      <Box
        width="100%"
        h="50px"
        backgroundColor="#026AA7"
        display="flex"
        alignItems="center"
      >
          <Text ml="40px" fontSize="22px" color="white" as="b">
            Trello
          </Text>
      </Box>
      
    );
  }
}

//Api Key: d2c8acd71fe432e8a650dd6693474e40

//Token: e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c
