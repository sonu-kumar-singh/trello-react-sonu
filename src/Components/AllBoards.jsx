import React, { Component } from 'react';
import * as BoardsApi from './Api';
import { Box, Button, Text, Input, Toast } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from '@chakra-ui/react';

export default class AllBoards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boards: [],
      isLoading: true,
      hasError: false,
      isOpen: false,
      toastOpen: false,
      newBoard: '',
    };
  }

  componentDidMount() {
    BoardsApi.getAllBoards()
      .then(boards => {
        this.setState({
          boards: boards,
          isLoading: false,
          hasError: false,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false,
          hasError: true,
        });
      });
  }

  onOpen = () => {
    this.setState({
      isOpen: true,
    });
  };

  onClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    try {
      const newBoard = await BoardsApi.createBoard(this.state.newBoard);
      this.state.boards.push(newBoard);
      this.setState({
        boards: this.state.boards,
        isOpen: false,
        toastOpen: true,
        newBoard: '',
      });
    } catch (err) {
      this.setState({
        hasError: true,
      });
    }
  };

  render() {
    const { boards, isLoading, hasError } = this.state;
    if (isLoading) {
      return <Box fontSize="30px">Loading...</Box>;
    }

    if (hasError) {
      return (
        <Box fontSize="20px">Error occured while fetching the boards data</Box>
      );
    }

    return (
      <>
        {this.state.toastOpen &&
          Toast({
            title: 'Account created.',
            description: "We've created your account for you.",
            status: 'success',
            duration: 1000,
            isClosable: true,
          })}

        <Box fontSize="2xl" m="50px">
          Your Workspaces
        </Box>
        <Box display="flex" flexWrap="wrap" mx="30px">
          {boards.map((board, index) => {
            return (
              <Link to={`/boards/${board.id}`} key={index}>
                <Box
                  w="280px"
                  h="150px"
                  m="20px"
                  borderRadius="5px"
                  fontSize="2xl"
                  backgroundRepeat="no-repeat"
                  color="white"
                  backgroundColor="Highlight"
                  backgroundSize="cover"
                  backgroundImage={`url(${board.prefs.backgroundImage})`}
                >
                  {board.name}
                </Box>
              </Link>
            );
          })}
        </Box>

        <Box
          onClick={this.onOpen}
          w="250px"
          h="120px"
          my="20px"
          ml="50px"
          borderRadius="5px"
          backgroundColor="#D3D3D3"
          display="flex"
          justifyContent="center"
          alignItems="center"
          style={{ cursor: 'pointer' }}
        >
          <Text> create new board </Text>
        </Box>

        <Modal isOpen={this.state.isOpen}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Create board</ModalHeader>
            <ModalBody>
              <form onSubmit={this.handleSubmit}>
                <Text>Board Title:</Text>
                <Input
                  type="text"
                  my="5px"
                  value={this.state.newBoard}
                  onChange={e => {
                    this.setState({ newBoard: e.target.value });
                  }}
                />
                <Input type="submit" my="10px" w="200px" />
              </form>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={this.onClose}>
                Close
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    );
  }
}
