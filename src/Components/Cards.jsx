import React, { Component } from 'react';
import * as TrelloApi from './Api';
import { Box, Text, Input, Button } from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';
import CheckList from './CheckList';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from '@chakra-ui/react';
export default class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listId: props.listId,
      cards: [],
      isLoading: true,
      hasError: false,
      isOpen: false,
      checkListIsOpen: false,
      newCheckList: '',
      cardModelIndex: '',
      newCreatedCard: '',
    };
  }

  componentDidMount() {
    TrelloApi.getAllCards(this.state.listId)
      .then(res => {
        this.setState({
          cards: res,
          isLoading: false,
          hasError: false,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false,
          hasError: true,
        });
      });
  }

  onOpen = index => {
    this.setState({
      isOpen: true,
      cardModelIndex: index,
    });
  };

  onClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  checkListOpenFunc = () => {
    this.setState({
      checkListIsOpen: true,
    });
  };

  checkListCloseFunc = () => {
    this.setState({
      checkListIsOpen: false,
      newCheckList: '',
    });
  };

  createCardFunc = async event => {
    event.preventDefault();
    try {
      const newCard = await TrelloApi.createNewCard(
        this.state.newCreatedCard,
        this.state.listId
      );
      const modifiedCards = this.state.cards;
      modifiedCards.push(newCard);
      this.setState({
        cards: modifiedCards,
        isLoading: false,
        hasError: false,
        newCreatedCard: '',
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  };

  handleCardDeletion = async index => {
    try {
      await TrelloApi.deleteCard(this.state.cards[index].id);

      const newCards = this.state.cards.filter(
        card => card.id !== this.state.cards[index].id
      );
      this.setState({
        cards: newCards,
        isLoading: false,
        hasError: false,
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  };

  handleCreatecheckListFunc = async (e, cardId) => {
    e.preventDefault();
    try {
      const newCheckList = await TrelloApi.createCheckList(
        cardId,
        e.target['0'].value
      );
      this.setState({
        isLoading: false,
        hasError: false,
        newCheckList: newCheckList,
      });
      e.target['0'].value = ''
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  };

  render() {
    const { cards, isLoading, hasError } = this.state;
    if (isLoading) {
      return <Box>Loading...</Box>;
    }

    if (hasError) {
      return <Box>Error occured while fetching</Box>;
    }

    return (
      <Box display="flex" mx="10px" flexDirection="column" alignItems="center">
        {cards.map((card, index) => {
          return (
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
              my="5px"
              borderRadius="5px"
              backgroundColor="white"
              key={card.id}
            >
              <Box
                w="250px"
                onClick={() => {
                  this.onOpen(index);
                }}
                style={{ cursor: 'pointer' }}
              >
                <Text fontSize="15px" ml="10px" my="9px" display="inline-block">
                  {card.name}
                </Text>
                {index === this.state.cardModelIndex && (
                  <Modal isOpen={this.state.isOpen} size="xl">
                    <ModalOverlay />
                    <ModalContent>
                      <ModalHeader>{card.name}</ModalHeader>
                      <ModalBody>
                        <CheckList
                          cardId={card.id}
                          newCheckList={this.state.newCheckList}
                        />

                        <Button
                          ml="5px"
                          as="b"
                          my="15px"
                          onClick={this.checkListOpenFunc}
                          border="2px"
                          style={{ cursor: 'pointer' }}
                          borderRadius="5px"
                          backgroundColor=" #04AA6D"
                        >
                          check list
                          <Modal isOpen={this.state.checkListIsOpen} isCentered>
                            <ModalOverlay />
                            <ModalContent>
                              <ModalHeader>Add check list</ModalHeader>
                              <ModalBody>
                                <form
                                  onSubmit={e => {
                                    this.handleCreatecheckListFunc(e, card.id);
                                  }}
                                >
                                  <Text>Check List Title:</Text>
                                  <Input
                                    type="text"
                                    my="5px"
                                  />
                                  <Button type="submit" my="10px" w="200px">
                                    Add
                                  </Button>
                                </form>
                              </ModalBody>

                              <ModalFooter>
                                <Button
                                  colorScheme="blue"
                                  mr={3}
                                  onClick={this.checkListCloseFunc}
                                >
                                  Close
                                </Button>
                              </ModalFooter>
                            </ModalContent>
                          </Modal>
                        </Button>
                      </ModalBody>

                      <ModalFooter>
                        <Button
                          colorScheme="blue"
                          mr={3}
                          onClick={this.onClose}
                        >
                          Close
                        </Button>
                      </ModalFooter>
                    </ModalContent>
                  </Modal>
                )}
              </Box>
              <DeleteIcon
                mr="10px"
                onClick={() => {
                  this.handleCardDeletion(index);
                }}
                style={{ cursor: 'pointer' }}
              />
            </Box>
          );
        })}

        <form
          onSubmit={event => {
            this.createCardFunc(event);
          }}
        >
          <Text color="black" fontSize="17px" as="b" ml="6px">
            Enter Card Title:
          </Text>
          <Input
            type="text"
            my="5px"
            ml="8px"
            w="250px"
            backgroundColor="white"
            value={this.state.newCreatedCard}
            onChange={e => {
              this.setState({ newCreatedCard: e.target.value });
            }}
            border="2px"
            placeholder="card title"
            required
          />
          <Button
            type="submit"
            my="9px"
            w="100px"
            ml="80px"
            backgroundColor=" #04AA6D"
          >
            {' '}
            create card{' '}
          </Button>
        </form>
      </Box>
    );
  }
}
