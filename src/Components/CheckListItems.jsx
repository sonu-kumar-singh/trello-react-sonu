import React, { Component } from 'react';
import * as TrelloApi from './Api';
import { Box, Input, Button, Checkbox, Stack } from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';
class CheckListItems extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkListItems: [],
      isLoading: true,
      hasError: false,
      newCheckItem: '',
    };
  }

  componentDidMount() {
    TrelloApi.getCheckItems(this.props.checkListId)
      .then(res => {
        this.setState({
          checkListItems: res,
          isLoading: false,
          hasError: false,
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false,
          hasError: true,
        });
      });
  }

  handleCheckItemCreation = async e => {
    e.preventDefault();
    try {
      const newCheckItem = await TrelloApi.createNewCheckItem(
        this.state.newCheckItem,
        this.props.checkListId
      );

      this.state.checkListItems.push(newCheckItem);
      this.setState({
        checkListItems: this.state.checkListItems,
        newCheckItem: '',
        isLoading: false,
        hasError: false,
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  };

  handleCheckListItemDeletion = async checkItemId => {
    try {
      await TrelloApi.deleteCheckItem(this.props.checkListId, checkItemId);

      const modifiedCheckListItems = this.state.checkListItems.filter(
        checkListItem => checkListItem.id !== checkItemId
      );
      this.setState({
        checkListItems: modifiedCheckListItems,
        isLoading: false,
        hasError: false,
      });
    } catch (err) {
      this.setState({
        hasError: true,
      });
    }
  };

  render() {
    const { checkListItems } = this.state;
    if (this.state.isLoading) {
      return <Box>Loading...</Box>;
    }

    if (this.state.hasError) {
      return <Box>Error occured while fetching checkitems</Box>;
    }

    return (
      <>
        <Stack spacing={5} direction="column">
          {checkListItems.length > 0 &&
            checkListItems.map(checkListItem => {
              return (
                <>
                  <Box display="flex" my="5px" justifyContent="space-between">
                    <Checkbox colorScheme="red">{checkListItem.name}</Checkbox>
                    <DeleteIcon
                      onClick={() => {
                        this.handleCheckListItemDeletion(checkListItem.id);
                      }}
                      style={{ cursor: 'pointer' }}
                    />
                  </Box>
                </>
              );
            })}
        </Stack>

        <form onSubmit={this.handleCheckItemCreation}>
          <Input
            type="text"
            my="5px"
            placeholder="Add an item"
            value={this.state.newCheckItem}
            onChange={e => {
              this.setState({ newCheckItem: e.target.value });
            }}
            required
          />
          <Button type="submit" my="5px">
            Add
          </Button>
        </form>
      </>
    );
  }
}

export default CheckListItems;
