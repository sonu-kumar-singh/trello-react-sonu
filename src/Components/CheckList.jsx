import React, { Component } from 'react';
import * as TrelloApi from './Api';
import { Box } from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';
import CheckListItems from './CheckListItems';
export default class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkLists: [],
      isLoading: true,
      hasError: false,
    };
  }
  componentDidMount() {
    TrelloApi.getAllCheckLists(this.props.cardId)
      .then(res =>
        this.setState({
          checkLists: res,
          isLoading: false,
          hasError: false,
        })
      )
      .catch(err => {
        this.setState({
          isLoading: false,
          hasError: true,
        });
      });
  }

  static getDerivedStateFromProps(props, state) {
    if (props.newCheckList !== '') {
      const checkLists = state.checkLists;
      checkLists.push(props.newCheckList);
      return { checkLists: checkLists, isLoading: false, hasError: false };
    }
    return null;
  }

  handleCheckListDeletion = async checkListId => {
    try {
      await TrelloApi.deleteCheckList(checkListId);
      const newCheckLists = this.state.checkLists.filter(
        checkList => checkList.id !== checkListId
      );
      this.setState({
        checkLists: newCheckLists,
        isLoading: false,
        hasError: false,
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  };

  render() {
    if (this.state.isLoading) {
      return <Box>Loading...</Box>;
    }

    if (this.state.hasError) {
      return <Box>Error occured while fetching</Box>;
    }

    if (this.state.checkLists.length > 0) {
      return (
        <>
          {this.state.checkLists.map(checkList => {
            return (
              <>
                <Box
                  h="40px"
                  w="100%"
                  backgroundColor=""
                  display="flex"
                  alignItems="center"
                  justifyContent="space-between"
                >
                  <Box my="2px" display="inline-block" as='b' >
                    {checkList.name}
                  </Box> 
                  <DeleteIcon
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      this.handleCheckListDeletion(checkList.id);
                    }}
                  />
                </Box>
                <CheckListItems checkListId={checkList.id} />
               
              </>
            );
          })}
        </>
      );
    } else return null;
  }
}
