import axios from 'axios';

export const getAllBoards = () => {
  return axios
    .get(
      'https://api.trello.com/1/members/me/boards?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c'
    )
    .then(res => res.data);
};

export const getOneBoard = boardId => {
  return axios
    .get(
      `https://api.trello.com/1/boards/${boardId}?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const getAllLists = boardId => {
  return axios
    .get(
      `https://api.trello.com/1/boards/${boardId}/lists?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const getAllCards = listId => {
  return axios
    .get(
      `https://api.trello.com/1/lists/${listId}/cards?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const createBoard = boardName => {
  return axios
    .post(
      `https://api.trello.com/1/boards/?name=${boardName}&key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const createNewList = (listName, boardId) => {
  return axios
    .post(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const createNewCard = (cardName, listId) => {
  return axios
    .post(
      `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const deleteList = listId => {
  return axios
    .put(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const deleteCard = cardId => {
  return axios
    .delete(
      `https://api.trello.com/1/cards/${cardId}?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const getAllCheckLists = cardId => {
  return axios
    .get(
      `https://api.trello.com/1/cards/${cardId}/checklists?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const createCheckList = (cardId, checkListName) => {
  return axios
    .post(
      `https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListName}&key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const deleteCheckList = checkListId => {
  return axios
    .delete(
      `https://api.trello.com/1/checklists/${checkListId}?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const getCheckItems = checkListId => {
  return axios
    .get(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const createNewCheckItem = (newCheckItemName, checkListId) => {
  return axios
    .post(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${newCheckItemName}&key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};

export const deleteCheckItem = (checkListId, checkItemId) => {
  return axios
    .delete(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=d2c8acd71fe432e8a650dd6693474e40&token=e8bff5edb708d39736694379b1e69c021304ba4ede10da4652dacb47aaf0704c`
    )
    .then(res => res.data);
};
