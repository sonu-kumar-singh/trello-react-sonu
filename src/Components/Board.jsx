import React, { Component } from 'react';
import * as BoardsApi from './Api';
import { Link } from 'react-router-dom';
import { Box, Text, Input, Button } from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';
import Cards from './Cards';

export default class Board extends Component {
  constructor(props) {
    super(props);

    this.state = {
      board: {},
      Lists: [],
      isLoading: true,
      hasError: false,
      newList: '',
    };
  }
  async componentDidMount() {
    const boardId = this.props.match.params.boardId;
    try {
      const board = await BoardsApi.getOneBoard(boardId);
      const Lists = await BoardsApi.getAllLists(boardId);
      this.setState({
        board: board,
        Lists: Lists,
        isLoading: false,
        hasError: false,
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  }

  handleSubmit = async e => {
    e.preventDefault();
    try {
      const newList = await BoardsApi.createNewList(
        this.state.newList,
        this.props.match.params.boardId
      );

      this.state.Lists.push(newList);
      this.setState({
        Lists: this.state.Lists,
        newList: '',
      });
    } catch (err) {
      this.setState({
        hasError: true,
      });
    }
  };

  deleteListFunc = async listId => {
    try {
      const deletedList = await BoardsApi.deleteList(listId);
      console.log(deletedList);
      const newLists = this.state.Lists.filter(List => List.id !== listId);
      this.setState({
        Lists: newLists,
        isLoading: false,
        hasError: false,
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        hasError: true,
      });
    }
  };

  render() {
    const { board, Lists, isLoading, hasError } = this.state;

    if (isLoading) {
      return <Box fontSize="30px">Loading...</Box>;
    }

    if (hasError) {
      return (
        <Box fontSize="20px">Error occured while fetching the board data</Box>
      );
    }

    if (Object.keys(board).length > 0) {
      return (
        <>
          <Box fontSize="30px" py="5px" pl="40px" backgroundColor="Grey">
            {board.name}
          </Box>
          <Link to="/boards">
            <Button my="5px" ml="40px">
              Back
            </Button>
          </Link>
          <Box
            w="100%"
            h="625px"
            fontSize="2xl"
            color="white"
            backgroundRepeat="no-repeat"
            backgroundSize="cover"
            backgroundImage={`url(${board.prefs.backgroundImage})`}
          >
            <Box display="flex" flexWrap="wrap">
              {Lists.map((List, index) => {
                return (
                  <Box
                    key={List.id}
                    borderRadius="5px"
                    width="300px"
                    m="5px"
                    backgroundColor="#D3D3D3"
                    color="black"
                  >
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Text fontSize="20px" as="b" py="5px" ml="10px">
                        {List.name}
                      </Text>
                      <DeleteIcon
                        py="5px"
                        fontSize="30px"
                        mr="10px"
                        onClick={() => {
                          this.deleteListFunc(List.id, index);
                        }}
                        style={{ cursor: 'pointer' }}
                      />
                    </Box>
                    <Cards listId={List.id} />
                  </Box>
                );
              })}
              <Box
                w="300px"
                m="5px"
                backgroundColor="#D3D3D3"
                borderRadius="5px"
              >
                <form
                  onSubmit={e => {
                    this.handleSubmit(e);
                  }}
                >
                  <Text
                    color="black"
                    fontSize="20px"
                    py="12px"
                    as="b"
                    ml="10px"
                  >
                    Enter List Title:
                  </Text>
                  <Input
                    type="text"
                    my="6px"
                    w="250px"
                    ml="25px"
                    color="black"
                    backgroundColor="white"
                    value={this.state.newList}
                    onChange={e => {
                      this.setState({ newList: e.target.value });
                    }}
                    border="2px"
                    placeholder="List title"
                    title
                    required
                  />
                  <Button
                    type="submit"
                    my="9px"
                    w="100px"
                    ml="100px"
                    backgroundColor=" #04AA6D"
                  >
                    create list
                  </Button>
                </form>
              </Box>
            </Box>
          </Box>
        </>
      );
    }
  }
}
