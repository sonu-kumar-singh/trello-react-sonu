import React from 'react';
import { ChakraProvider, theme } from '@chakra-ui/react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './Components/Header';
import AllBoards from './Components/AllBoards';
import Board from './Components/Board';

function App() {
  return (
    <ChakraProvider theme={theme}>
      <Header />
      <Router>
        <Switch>
          <Route exact path="/boards" component={AllBoards} />
          <Route path="/boards/:boardId" component={Board} />
        </Switch>
      </Router>
    </ChakraProvider>
  );
}

export default App;
